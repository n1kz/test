//import org.testng.ITestContext;
//import org.testng.ITestListener;
//import org.testng.ITestResult;
//import ru.yandex.qatools.allure.model.TestCaseResult;
//
//import java.lang.reflect.Method;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//public class EventListener implements ITestListener {
//
//    public static Logger LOGGER = Logger.getLogger("");
//
//    @Override
//    public void onTestStart(ITestResult iTestResult) {
//        LOGGER.log(Level.INFO, "Тест стартовал " + iTestResult.getMethod().getDescription());
//    }
//
//    @Override
//    public void onTestSuccess(ITestResult tr) {
//        LOGGER.log(Level.INFO, "Тест завершился успешно " + tr.getMethod().getDescription());
//        tr.getMethod().
//        addTestResult(tr, new TestCaseResult().setStatusId(1));
//    }
//
//    @Override
//    public void onTestFailure(ITestResult tr) {
//        String testDescription = tr.getMethod().getDescription();
//        LOGGER.log(Level.INFO, "Тест завершился c ошибкой " + testDescription);
//
//        addTestResult(tr, new TestCaseResult().setStatusId(5));
//        addAttachment(tr, testDescription);
//
//    }
//
//    @Override
//    public void onTestSkipped(ITestResult tr) {
//        String testDescription = tr.getMethod().getDescription();
//        addAttachment(tr, testDescription);
//
//        LOGGER.log(Level.INFO, "Тест скипнулся " + tr.getMethod().getDescription());
//        addTestResult(tr, new TestCaseResult().setStatusId(2));
//    }
//
//    @Override
//    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
//
//    }
//
//    @Override
//    public void onStart(ITestContext iTestContext) {
//    }
//
//    @Override
//    public void onFinish(ITestContext iTestContext) {
//    }
//
//    private int[] getTestRailCaseId(ITestResult iTestResult) {
//        Method method = iTestResult.getMethod().getMethod();
//        TestRail testRailAnnotation = method.getAnnotation(TestRail.class);
//        return testRailAnnotation.caseId();
//    }
//
//    @Attachment(value = "{0}", type = "image/png")
//    public static byte[] saveScreenshot(String description, WebDriver webDriver) {
//        return ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES);
//    }
//
//    private void addAttachment(ITestResult tr, String testDescription) {
//            Object testClass = tr.getInstance();
//            AppiumDriver<MobileElement> driver = ((BaseTest) testClass).getDriver();
//            saveScreenshot(testDescription, driver);
//    }
//
//    private void addTestResult(ITestResult tr, TestCaseResult testCaseResult) {
//        for (int testCaseId : getTestRailCaseId(tr)) {
//            testCaseResult(testRunId, testCaseId, testCaseResult);
//        }
//    }
//}