//import com.github.tomakehurst.wiremock.WireMockServer;
//import com.github.tomakehurst.wiremock.junit.WireMockRule;
//import io.restassured.RestAssured;
//import io.restassured.http.Header;
//import io.restassured.response.Response;
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//import static com.github.tomakehurst.wiremock.client.WireMock.*;
//
//public class SimpleTest {
//    WireMockServer wireMockServer = new WireMockServer();
//
//    @BeforeClass
//   public void startServer() {
//
//        wireMockServer.start();
//   }
//
//   @AfterClass
//   public void closeSever(){
//        wireMockServer.stop();
//   }
//
//
//    //parsing all request for listening address
//    @Test
//    public void FirthTest() {
//        stubFor(post(urlEqualTo("/api/v1/auth/loginl"))
//                //.withHeader("Accept", equalTo("text/xml"))
//                .willReturn(aResponse()
//                        .withStatus(200)
//                        .withHeader("Content-Type", "text/xml")
//                        .withBody("<response>Some content</response>")
//                ));
//
//        //rest-assured create post request for testing mock service and send request on mock service
//        Response response = RestAssured.given().
//                header(new Header("Content-Type", "application/json")).
//                body("{name: 'test'}")
//                .post("http://localhost:8089/my/resource");
//        response.statusLine();
//        System.out.println(response.headers().toString());
//        response.body().prettyPrint();
//
//        //verify request
//        verify(postRequestedFor(urlMatching("/my/resource"))
//                .withRequestBody(matching("\\{name: .*\\}"))
//                .withHeader("Content-Type", notMatching("application/json")));
//    }
//}