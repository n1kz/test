import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.common.ProxySettings;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class Server {


    @BeforeClass
    public static void main(String[] args) {


       // WireMockServer mockServer = new WireMockServer(WireMockConfiguration.wireMockConfig().proxyVia(new ProxySettings("192.168.56.1",8887)).bindAddress("192.168.56.1").port(8887));
        WireMockServer mockServer = new WireMockServer(WireMockConfiguration.wireMockConfig().bindAddress("192.168.56.1").port(8887));
        mockServer.start();

//        mockServer.stubFor(get(urlMatching(".*"))
//                .willReturn(aResponse()
//                                .proxiedFrom("https://kbrdev.mobiledimension.ru/api/v1")));


            mockServer.stubFor(get(urlMatching(".*")).atPriority(1)
                .willReturn(aResponse().withStatus(501)));




//        mockServer.stubFor(get(urlEqualTo("/api/v1/sync/check")).atPriority(1)
//                .willReturn(aResponse().withStatus(503)));

    }
}


