package ru.job.alfabank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.base.Functions.identity;
import static java.util.Arrays.stream;
import static java.util.Comparator.reverseOrder;

public class alfa {
        public static void main(String[] args) throws IOException {
            List<Integer> list = Files.lines(Paths.get("C:/Users/nsidyakin/Desktop/mRTD/test.txt"))
                    .map(s -> stream(s.split(",")))
                    .flatMap(identity())
                    .map(Integer::parseInt)
                    .sorted()
                    .collect(Collectors.toList());
            list.stream()
                    .map(i -> i + " ").forEach(System.out::print);
            list.stream()
                    .sorted(reverseOrder())
                    .map(i -> i + " ")
                    .forEach(System.out::print);
    }
}
